package gps.a20201313638.testingrepo.calculator;

public class calculatorSimpleFuncs {
    public static double add(double a, double b){
        return a+b;
    }

    public static double subtract(double a, double b){
        return a-b;
    }

    public static double multiplicate(double a, double b){
        return a*b;
    }

    public static double divide(double a, double b){
        if(b!=0) return a/b;
        else return 0.0;
    }

    public static double invertSignal(double a){
        return a*(-1);
    }

    public static double becomePercentage(double a){
        return a/100;
    }

    public static double square(double a){
        return a*a;
    }

    public static double invertFrac(double a){
        return 1/a;
    }
    public static double invertFrac(double a, double b){
        return b/a;
    }
}
