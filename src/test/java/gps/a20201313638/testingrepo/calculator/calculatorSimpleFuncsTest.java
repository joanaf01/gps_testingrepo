package gps.a20201313638.testingrepo.calculator;

import static org.junit.jupiter.api.Assertions.*;

class calculatorSimpleFuncsTest {

    @org.junit.jupiter.api.Test
    void add() {
        double result = calculatorSimpleFuncs.add(1.2,2.1);
        double correct = 3.3;

        assertEquals(correct,result);
    }

    @org.junit.jupiter.api.Test
    void subtract() {
        assertEquals(1.8,calculatorSimpleFuncs.subtract(3,1.2));
    }

    @org.junit.jupiter.api.Test
    void multiplicate() {
        assertEquals(4.5, calculatorSimpleFuncs.multiplicate(1.5,3));
    }

    @org.junit.jupiter.api.Test
    void divide() {
        assertEquals(2.0,calculatorSimpleFuncs.divide(6,3));
        assertEquals(0,calculatorSimpleFuncs.divide(3,0));
    }

    @org.junit.jupiter.api.Test
    void invertSignal() {
        assertEquals(-1,calculatorSimpleFuncs.invertSignal(1));
        assertEquals(1,calculatorSimpleFuncs.invertSignal(-1));
    }

    @org.junit.jupiter.api.Test
    void becomePercentage() {
        assertEquals(0.8,calculatorSimpleFuncs.becomePercentage(80));
    }

    @org.junit.jupiter.api.Test
    void square() {
        assertEquals(9,calculatorSimpleFuncs.square(3));
    }

    @org.junit.jupiter.api.Test
    void invertFrac() {
        assertEquals(0.5,calculatorSimpleFuncs.invertFrac(2));
        assertEquals(0.5,calculatorSimpleFuncs.invertFrac(4,2));
    }
}